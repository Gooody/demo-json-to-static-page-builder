

// external modules declaration
const App = require('./app/bootstrap');
const express = require('express');
const multer = require('multer');
const mkdirp = require('mkdirp');
const async = require('async');
const request = require('request');
const fs = require('fs');
const cors = require('cors');

// init Express
let expressApp = express();
expressApp.use(express.json());
expressApp.use(express.urlencoded({ extended: true }));

// Add CORS
expressApp.use(cors());

// init default values
const uploads  = process.env.UPLOADS || __dirname + '/tmp/uploads/';
const input  = process.env.INPUT || __dirname +'/tmp/input/';
const output = process.env.OUTPUT || __dirname + '/tmp/output/';
const imgCache = process.env.IMG_CACHE || __dirname + '/tmp/img_cache/';
const contentParams = ['pages','json', 'css', 'js'];

// init data infornation for upload data
const upload = multer({ dest: uploads });
var cpUpload = upload.fields([
    { name: 'file_json[]', maxCount:10 },
    { name: 'file_html[]', maxCount:10 },
    { name: 'file_css[]' , maxCount:10 },
    { name: 'file_js[]' , maxCount:10 }
]);

//checking s3 access keys
if (!process.env.DEV_ONLY && (!process.env.S3_ACCESS_KEY || !process.env.S3_SECRET)) {
    return console.error('ENV S3_ACCESS_KEY and S3_SECRET cannot be empty');
}

/*
 * functionality for bridge data (POST or JSON)
 */
const jumper = function(req, res, params) {
    //prepare siteId
    const siteId = params.site_id;
    //generation tmp folder name
    const tmpName = (new Date().getTime()) +'-'+ (Math.floor(Math.random() * 100000)) + '/';
    //generation tmp input folder name
    const tmpInput = input + tmpName;
    //generation tmp output folder name
    const tmpOutput = output + tmpName;
    //generation tmp webpack folder name
    const tmpWebpackOutput = (process.env.WEBPACK ? process.env.WEBPACK + tmpName : false) || (tmpOutput + 'webpack/');
    //prepare external js information
    const externalJs = params.external_js || [];
    //prepare external css information
    const externalCss = params.external_css || [];

    //checking s3 bucket name
    if (!params.s3_bucket) {
        res.status(500);
        res.send({status:'error', site_id:siteId, error: 'Error params'});
        return;
    }

    //generation configuration for generation
    const appConfig = {
        //path to the input folder
        input: tmpInput,

        //path to the output folder
        output: tmpOutput,

        //path to the image cache folder
        imgCache: imgCache,

        //Id of the site
        siteId:siteId,

        //webpack mode for generation
        webpackMode: process.env.WEBPACK_MODE || 'production',

        //path to the webpack data folder
        webpackDist: tmpWebpackOutput,

        //base template
        template: './templates/' + (params.template || process.env.TEMPLATE || 'base.html'),

        //html class name, used for replace attr style
        generateClassName: params.generated_class_name || process.env.GENERATED_CLASS_NAME || 'custom-generated',
        src:{
            //path to the main html file
            internal_html: params.html_path || process.env.HTML_PATH || 'index.html',

            //path to the main js file
            internal_js: params.js_path || process.env.JS_PATH || 'js/view.js',

            //path to the main css file
            internal_css: params.css_path || process.env.CSS_PATH || 'css/custom.css',

            //path to the images folder
            internal_image: params.image_path || process.env.IMAGE_PATH || 'image/',

            //path to the file robots.txt
            internal_robots: 'robots.txt',

            //array of the external js-libraries urls
            external_js: Array.isArray(externalJs) ? externalJs : [externalJs],

            //array of the external css-libraries urls
            external_css: Array.isArray(externalCss) ? externalCss : [externalCss]
        },

        //array of the extra modules names
        extraModules: params.extra_modules || [],

        //pages public url
        publicPath: params.public_path || '',

        //robots.txt content
        robotsText: params.robots_text || '',

        //id of the error page
        errorPageId: params.error_page_id || false,

        //id of the index page
        indexPageId: params.index_page_id || false,

        //s3 access configuration
        s3:{
            accessOptions: {
                accessKeyId: process.env.S3_ACCESS_KEY,
                secretAccessKey: process.env.S3_SECRET
            },
            bucketParams: {
                Bucket: params.s3_bucket,
                Prefix: params.s3_prefix
            }
        }
    };

    //creation input folder
    mkdirp.sync(tmpInput);

    //saving POST data
    async.eachOfLimit(req.files || [], 10, function(files, it, asyncCbFiles) {
        async.eachOfLimit(files, 10, function(file, iteration, asyncCb) {
            let oldPath = file.destination + file.filename;
            let newPath = tmpInput + file.originalname;
            fs.rename(oldPath, newPath, function (err) {
                if (err) throw err
                asyncCb();
            });
        }, function() {
            asyncCbFiles();
        });

    }, function() {

        //init App for data-generation
        let app = new App(appConfig);

        //adding content of pages, json, css, js for processing
        for (let i in contentParams) {
            if ('undefined' != typeof params[contentParams[i]]) {
                let cntArr = Array.isArray(params[contentParams[i]]) ? params[contentParams[i]] : [params[contentParams[i]]];
                for (let ii in cntArr) {
                    app.setContent(contentParams[i], cntArr[ii], 'POST-param-'+contentParams[i]+'-'+ii );
                }
            }
        }

        //adding content params of html for processing
        if ('undefined' != typeof params['html']) {
            for (let name in params['html']) {
                let cntArr = Array.isArray(params['html'][name]) ? params['html'][name] : [params['html'][name]];
                for (let ii in cntArr) {
                    app.setContent('html', cntArr[ii], name);
                }
            }
        }

        //functionality for send error response
        const sendError = function (err, errText) {
            res.status(500);
            res.send({status:'error', site_id:siteId, error: errText, text:err });
        };

        //functionality for send success response
        const sendSuccess = function () {
            res.send({status:'done', site_id:siteId});
        };

        //functionality for pushing data to S3
        const pushToS3 = function (errData) {
            if (process.env.DEV_ONLY) {
                console.log('Building done');
                sendSuccess();
                return;
            }
            app.pushToS3().then(function() {
                app.clear();
                if (errData) {
                    sendError(errData.body, errData.text);
                    return
                }
                sendSuccess();
            }, function(pushToS3Err){
                console.error('Push To S3 Error', pushToS3Err);
                pushToS3Err = errData ? [errData, pushToS3Err] : [pushToS3Err];
                sendError(pushToS3Err, 'Push To S3 Error');
            });
        };

        //start processing
        app.run().then(function(){
            //building processing
            app.build().then(function() {
                pushToS3();
            }, function(buildErr) {
                console.error('Build Error', buildErr);
                pushToS3( {text: 'Build Error', body:buildErr} );
            });
        }, function(runErr){
            console.error('Run Error', runErr);
            sendError((runErr.text?runErr.text:null), 'Run Error');
        });
    });
};



//init route for POST data processing
expressApp.post('/', cpUpload, function (req, res) {
    jumper(req, res, req.body);
});

//init route for generate HTML by external JSON file
expressApp.get('/deploy-site/:siteId', function (req, res) {
    const siteId = req.params.siteId;
    const jsonUrl = process.env.JSON_STORAGE_URL + siteId;
    const options = {
        url:jsonUrl,
        json:true,
        method:"GET",
        strictSSL:false,
        headers: { 'Accept': 'application/json' }
    };

    // download JSON file
    request(options, function(error, response, body) {
        if (error) {
            console.error(error);
            res.status(500);
            res.send({status:'error', site_id:siteId, error: 'Request to Json Error'});
            return false;
        }
        let pages = Array.isArray(body.pages) ? body.pages : [body.pages];

        if (body.pages && pages.length) {

            // configure generation params
            const params = {
                site_id: body.id || siteId,
                error_page_id: body.error_page_id,
                index_page_id: body.index_page_id,
                external_js: [
                    "https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js",
                    "https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js",
                    "https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js",
                    "https://cdnjs.cloudflare.com/ajax/libs/wow/1.1.2/wow.min.js"
                ],
                external_css:[
                    "https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css",
                    "https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.css",
                    "https://cdnjs.cloudflare.com/ajax/libs/animate.css/3.5.2/animate.min.css",
                ],
                //template:[],
                generated_class_name:[],
                extra_modules:['LazyLoad'],
                public_path:'/',
                robots_text:body.robots_text,
                s3_bucket:body.bucket_name,
                s3_prefix:'',
                json: Array.isArray(body.blocks) ? body.blocks : [body.blocks],
                html_path: 1 == pages.length ? pages[0].file_path+'.html' : null,
                pages: pages
            };

            let dataJson = {};
            for (var i in body) {
                if ('boolean' == typeof body[i] || 'string' == typeof body[i] || 'number' == typeof body[i]) {
                    dataJson[i] = body[i];
                }
            }

            params.json.push(dataJson);
            jumper(req, res, params);
        } else {
            console.error('Pages is empty');
            res.status(500);
            res.send({status:'error', site_id:siteId, error: 'Pages is empty'});
            return false;
        }

    });
});

// init 404-error route
expressApp.use(function(req, res, next){
    res.status(404);
    res.send({status:'error', error: 'Not found'});
});

//init web server
const port = process.env.PORT || 3000;
expressApp.listen(port, function() {
    console.log('Http listening on port', port+'!');
    console.log('http://localhost:'+port);
});

