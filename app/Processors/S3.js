/**
 * Class: S3
 * Extends: AbstractProcessors
 *
 * @method build
 */

const AbstractProcessors = require('./AbstractProcessors');
S3.prototype = new AbstractProcessors();

function S3()
{
    // external modules declaration
    const s3Lib = require('s3');
    const diff = require('deep-diff').diff;
    const assign = require('object-assign');

    // variable declaration
    const self = this;
    let client = null;
    let s3 = null;
    let pages = [];
    let indexPageName = null;
    let errorPageName = 'error.html';


    // sets up a public-read bucket policy
    function setPolicy (cb) {
        let bucket = self.config.s3.bucketParams.Bucket;
        let publicRead = {
            Sid: 'AddPublicReadPermissions',
            Effect: 'Allow',
            Principal: '*',
            Action: 's3:GetObject',
            Resource: 'arn:aws:s3:::' + bucket + '/*'
        };

        s3.getBucketPolicy({ Bucket: bucket }, function (err, data) {
            if (err && err.code !== 'NoSuchBucketPolicy') return cb(err)

            let newPolicy = { Statement: [] }
            let oldPolicy

            try {
                oldPolicy = JSON.parse(data.Policy)
            } catch (err) {}

            let found = false;

            if (oldPolicy) {
                newPolicy.Statement = oldPolicy.Statement.map(function (item) {
                    if (item.Sid === 'AddPublicReadPermissions') {
                        found = true
                        return publicRead
                    }
                    return item
                })
            }

            if (!found) newPolicy.Statement.push(publicRead)

            let dirty = diff(oldPolicy || {}, newPolicy, function (path, key) {
                if (key === 'Version') return true
            });

            if (dirty) {
                let policy = assign(oldPolicy || {}, newPolicy)
                s3.putBucketPolicy({ Bucket: bucket, Policy: JSON.stringify(policy) }, cb)
            } else {
                process.nextTick(cb)
            }
        });
    }

    /**
     * Initiation processor, prepare s3 client
     * @return this
     */
    this.init = function(){
        client = s3Lib.createClient({
            maxAsyncS3: 20,     // this is the default
            s3RetryCount: 3,    // this is the default
            s3RetryDelay: 1000, // this is the default
            multipartUploadThreshold: 20971520, // this is the default (20 MB)
            multipartUploadSize: 15728640, // this is the default (15 MB)
            s3Options: self.config.s3.accessOptions
        });
        s3 = client.s3;
    };

    /**
     * functionality for add pages, for use indexPageName and errorPageName in the BucketData
     * @return Promise
     */
    this.addPage = function(page) {
        if (page.id == self.config.indexPageId) {
            indexPageName = page.file_name;
        }

        if (page.id == self.config.errorPageId) {
            errorPageName = page.file_name;
        }
        pages.push(page);
        return this;
    };

    /**
     * Push to S3
     * @return Promise
     */
    this.push = function()
    {
        return new Promise(function(resolve, reject) {
            //update Bucket information
            const updateBucketData = function() {
                setPolicy(function(){
                    //Configure Website
                    params.WebsiteConfiguration = {
                        ErrorDocument: {
                            Key: errorPageName
                        },
                        IndexDocument: {
                            Suffix: indexPageName || self.config.src.internal_html || 'index.html'
                        }
                    };

                    s3.putBucketWebsite(params, function(err, data) {
                        if (err) {
                            return reject({putBucketWebsite: err, params:params})
                        } else {

                            let uploadParams = {
                                localDir: self.config.output,
                                deleteRemoved: true, // default false, whether to remove s3 objects
                                s3Params: self.config.s3.bucketParams
                            };
                            console.log('Uploading to S3 Bucket');
                            let uploader = client.uploadDir(uploadParams);
                            uploader.on('error', function(err) {
                                console.error("unable to sync:", err.stack);
                                reject(err.stack);
                            });

                            uploader.on('end', function() {
                                console.log("Media Sync done");
                                resolve();
                            });
                        }
                    });
                });
            };

            //checking Bucket
            let params = { Bucket: self.config.s3.bucketParams.Bucket };
            s3.headBucket(params, function(err, data) {
                if (err) {
                    //creating Bucket (if doesn't exists)
                    s3.createBucket(params, function(err, data) {
                        if (err) {
                            //console.error('Created S3 Bucket Error', {createBucketError: err, params:params});
                            return reject({createBucketError: err, params:params})
                        }
                        console.log('Created S3 Bucket', params.Bucket);
                        updateBucketData();
                    });
                } else {
                    updateBucketData();
                }
            });
        });
    };
}

module.exports = S3;
