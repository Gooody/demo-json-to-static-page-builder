/**
 * Class: Style
 * Extends: AbstractProcessors
 *
 * @method build
 */

const AbstractProcessors = require('./AbstractProcessors');
Style.prototype = new AbstractProcessors();

function Style()
{
    // add type of processing data
    this.entityType = 'css';

    // external modules declaration
    const purify = require("purify-css");
    const async = require('async');
    const CssParser = require("css");

    // variable declaration
    const self = this;
    const contentFiles = [];

    /**
     * Used for storing js and html files
     */
    this.addEntity = function(filePath, type) {
        if(self.processors.Js.entityType === type || self.processors.Html.entityType === type) {
            contentFiles.push(filePath);
        }
    };

    /**
     * Building  css
     * @return Promise
     */
    this.build = function()
    {
        return new Promise(function(resolve, reject) {
            if (self._items.length) {
                console.info('Building Styles');
                let css = '';
                let imports = [];

                //check and storing css rule @import
                //concat incoming css
                for (let i in self._items ) {
                    let item = self._items[i];
                    if (new RegExp('@import','g').test(item)) {
                        const simpleCssJson = CssParser.parse(item);
                        for (let ii in simpleCssJson.stylesheet.rules) {
                            if ('import' == simpleCssJson.stylesheet.rules[ii].type) {
                                if (!RegExp('fonts.googleapis.com','g').test(simpleCssJson.stylesheet.rules[ii].import)) {
                                    imports.push('@import '+simpleCssJson.stylesheet.rules[ii].import+';')
                                }
                            }
                        }
                    } else {
                        css += item;
                    }
                }
                css = imports.join(' ') +' '+ css;

                //minify CSS options
                var options = { minify: true, };


                //minify css
                purify(contentFiles, css, options, function (purifiedResult) {
                    const simpleCssJson = CssParser.parse(purifiedResult);
                    const checkRules = function(rules) {

                        //search used font-families and font-weights
                        return new Promise(function(resolve) {
                            async.eachOfLimit(rules, 1, function(rule, iteration, asyncCb) {
                                if(rule.declarations) {
                                    let props = {};
                                    for (let i in rule.declarations) {
                                        props[rule.declarations[i].property] = rule.declarations[i].value
                                    }

                                    //checking font-family information
                                    if('undefined' != typeof props['font-family'] || 'undefined' != typeof props['font-weight']) {
                                        self.processors.Html.addFont(props['font-family'], props['font-weight']);
                                    }

                                    //checking background-image information
                                    if('undefined' != typeof props['background-image']) {
                                        let imgUrl = /\((.*?)\)$/.exec(props['background-image']);
                                        let imgSize = {};

                                        if (imgUrl && 'undefined' != typeof imgUrl[1]) {
                                            imgUrl =imgUrl[1];

                                            //checking sizes for background-images
                                            if('undefined' != typeof props['background-size']) {
                                                let sizes = props['background-size'].split(' ');
                                                if (sizes[0] && /px/g.test(sizes[0])  ) {
                                                    imgSize.width = parseInt(sizes[0].replace(/\D+/, ''));
                                                }
                                                if (sizes[1] && /px/g.test(sizes[1])  ) {
                                                    imgSize.height = parseInt(sizes[1].replace(/\D+/, ''))
                                                }
                                            }

                                            for (let i in rule.declarations) {
                                                if ('background-image' == rule.declarations[i].property) {
                                                    //rule.declarations[i].value =  'url('+self.imagePath(imgUrl, self.config.src.internal_image_css, imgSize)+')';
                                                    rule.declarations[i].value =  'url('+self.imagePath(imgUrl, self.config.src.internal_image, imgSize)+')';
                                                }
                                            }
                                        }
                                    }
                                    asyncCb();

                                } else if(rule.rules) {
                                    checkRules(rule.rules).then(asyncCb);
                                } else {
                                    asyncCb();
                                }

                            }, function() {
                                resolve();
                            });
                        });
                    };
                    checkRules(simpleCssJson.stylesheet.rules).then(function() {
                        //finishing of minify css
                        purify(contentFiles, CssParser.stringify(simpleCssJson), options, function (purifiedResult) {
                            self._builded = purifiedResult;
                            resolve();
                        });
                    });
                });
            }
        });
    }
}




module.exports = Style;