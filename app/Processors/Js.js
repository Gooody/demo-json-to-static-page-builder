/**
 * Class: Js
 * Extends: AbstractProcessors
 *
 * @method build
 */

const AbstractProcessors = require('./AbstractProcessors');
Js.prototype = new AbstractProcessors();

function Js()
{
    // add type of processing data
    this.entityType = 'js';

    // external modules declaration
    const fs = require('fs');
    const async = require('async');

    // variable declaration
    const self = this;

    /**
     * Minify and save js
     * @return Promise
     */
    this.build = function()
    {

        return new Promise(function(resolve, reject) {
            console.info('Building Js');
            //load Extra Modules
            if (self.config.extraModules){
                let extraModules =Array.isArray(self.config.extraModules) ? self.config.extraModules : [self.config.extraModules];
                for (var i in extraModules) {
                    try {
                        //load Extra Module by name
                        let extraModule = require('../../extra_modules/'+extraModules[i]+'/index.js');
                        self._items.push('('+extraModule.toString()+').call()');
                    } catch (e) {
                        console.error('Extra Module', extraModules[i], 'Does not exists! (ignored)');
                    }
                }
            }

            //send all JS to the webpack for generation
            async.eachOfLimit(self._items, 10, function(item, iteration, asyncCb) {
                self.saveForWebpack(item).then(function() {
                    asyncCb();
                }, function(err){
                    reject(err);
                });
            }, function() {
                resolve();
            });
        });
    };

    this.save = function() {
        return new Promise(function(resolve, reject) {
            resolve();
        });
    };
}

module.exports = Js;
