/**
 * Class: Webpack
 * Extends: AbstractProcessors
 *
 * @method build
 */

const AbstractProcessors = require('./AbstractProcessors');
Webpack.prototype = new AbstractProcessors();

function Webpack()
{
    // add type of processing data
    this.entityType = 'webpack';

    // external modules declaration
    const webpack = require('webpack');
    const ProgressPlugin = require('webpack/lib/ProgressPlugin');
    const parsePath = require("parse-filepath");
    const rimraf = require("rimraf");

    // variable declaration
    const self = this;

    const build = function(fileApp) {
        //file path for webpack data
        let file = parsePath(self.config.publicPath + self.config.src.internal_js);
        let fileEntry = parsePath(self.config.src.internal_js);
        return new Promise(function(resolve, reject) {
            let entry = {};
            entry[fileEntry.name] = self.config.webpackDist+fileApp.basename ;

            //webpack configuration
            const webpackConfig = {
                //mode:'development',
                mode: self.config.webpackMode || 'production',
                entry: entry,
                output:{
                    path: self.config.output + fileEntry.dirname,
                    publicPath:file.dirname,
                    filename:'[name].js'
                },
                plugins: [ ],
                module: {
                    rules: [
                        {
                            test: /\.js$/,
                            exclude: /(node_modules|bower_components)/,
                            use: {
                                loader: 'babel-loader',
                                //options: {
                                    //presets: ['@babel/preset-env']
                                //}
                            }
                        }
                    ]
                }
            };
            //Runing webpack data
            webpack(webpackConfig).run(function(err, stats) {
                if (err) {
                    return reject(err);
                }
                resolve();
            });
        })
    };

    /**
     * Run Webpack builder
     * @return Promise
     */
    this.save = function()
    {
        self.config.src.internal_webpack = 'app.js';
        return new Promise(function(resolve, reject) {
            console.info('Building Webpack Data');
            let appJs = [];

            //prepare files for building
            for(var i in self._items) {
                appJs.push("require('./"+self._items[i].basename+"');");
            }

            //saving files for building in the Webpack
            self.saveForWebpack(appJs.join("\r\n")).then(function(file){
                //building in the Webpack
                build(file).then(function(){
                    if (!process.env.DEV_ONLY) {
                        rimraf.sync(self.config.webpackDist);
                    }
                    resolve();
                }, reject);
            }, function(err){
                reject(err);
            })
        });
    }
}

module.exports = Webpack;
