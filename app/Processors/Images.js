/**
 * Class: Images
 * Extends: AbstractProcessors
 *
 * @method build
 * @method save
 */

const AbstractProcessors = require('./AbstractProcessors');
Images.prototype = new AbstractProcessors();

function Images()
{
    // add type of processing data
    this.entityType = 'image';

    // external modules declaration
    const mkdirp = require('mkdirp');
    const async = require('async');
    const fs = require('fs');
    const Jimp = require('jimp');
    const rimraf = require("rimraf");
    const download = require('download-file');
    const imagemin = require('imagemin');
    const imageminPlugins = {
        Jpg : require('imagemin-jpegtran'),
        Png  : require('imagemin-pngquant'),
        Gif  : require('imagemin-gif2webp'),
        Svg  : require('imagemin-svgo')
    };

    // variable declaration
    const self = this;
    let tmpDirectories = {};

    /**
     * download, resize and minify images
     * @return Promise
     */
    this.save = function() {
        console.log('Processing images');
        return new Promise(function(resolve, reject) {
            //let images = [];
            errors = [];

            //minify image functionality
            let imgMin = function(img, file, cb) {
                if (!fs.existsSync(img)) {
                    errors.push('file "'+ img + '" does not exists');
                    cb();
                    return null;
                }
                const imgPath = self.config.output+self.config.src.internal_image;

                imagemin([img], imgPath, {
                    plugins: [
                        imageminPlugins.Jpg(),
                        imageminPlugins.Png(),
                        imageminPlugins.Gif({minimize:true}),
                        imageminPlugins.Svg({minifyStyles:true})
                    ]
                }).then(function(files){
                    fs.copyFile(img, file.local.tmpImg, function(err){
                        if (!err) {
                            cb();
                        }
                    });
                }, function(imageminErr) {
                    console.error(imageminErr);
                    cb();
                });
            };

            async.eachOfLimit(self._items, 10, function(file, iteration, asyncCb) {
                //prepare tmp information for saving downloaded image
                const tmlDir = self.config.output + 'tmp-' + file.local.directory;
                tmpDirectories[tmlDir] = true;
                mkdirp.sync(tmlDir);
                let tmpFilePath = tmlDir+file.local.filename;

                //check is file exists in the cache (if yes - use this file)
                if (fs.existsSync(file.local.tmpImg)) {
                    mkdirp.sync(self.config.output+self.config.src.internal_image);
                    fs.copyFile(file.local.tmpImg, self.config.output+self.config.src.internal_image+file.local.filename, function(){
                        asyncCb();
                    });
                    return null;
                }

                if ('.svg' == file.ext.toLowerCase()) {
                    // downloading svg file
                    var options = {
                        directory: tmlDir,
                        filename: file.local.filename
                    };

                    //download file
                    download(file.path, options, function(err){
                        if (err) {
                            reject(err);
                            errors.push(err);
                            return asyncCb();
                        }

                        //push file for minify
                        setTimeout(function(){
                            imgMin(tmpFilePath, file, asyncCb);
                        }, 100);
                    });
                } else {
                    // downloading and resize Jpg,Png,Gif files
                    Jimp.read(file.path)
                        .then(image => {
                            if (file.local.size && file.local.size.height && file.local.size.height) {
                                image.resize(file.local.size.height, file.local.size.height);
                            }
                            return image.quality(90).write(tmpFilePath, function(){
                                    //push file for minify
                                    imgMin(tmpFilePath, file, asyncCb);
                                });
                        })
                        .catch(err => {
                            errors.push(err);
                            asyncCb();
                        });
                }
            }, function() {
                //clear tmp images
                for(let i in tmpDirectories) {
                    rimraf.sync(i);
                }
                if (errors.length) {
                    reject(errors);
                    return null;
                }
                resolve();
            });
        });
    };
}

module.exports = Images;
