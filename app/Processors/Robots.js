/**
 * Class: Js
 * Extends: AbstractProcessors
 *
 * @method build
 */

const AbstractProcessors = require('./AbstractProcessors');
Robots.prototype = new AbstractProcessors();

function Robots()
{
    // add type of processing data
    this.entityType = 'robots';

    // variable declaration
    const self = this;

    /**
     * Build Robots.txt by incoming data
     * @return Promise
     */
    this.build = function()
    {
        return new Promise(function(resolve, reject) {
            self._builded = self.config.robotsText;
            resolve();
        });
    };

}

module.exports = Robots;
