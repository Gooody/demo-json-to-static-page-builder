/**
 * Class: Html
 * Extends: AbstractProcessors
 *
 * @method addPage
 * @method addField
 * @method build
 * @method save
 */

const AbstractProcessors = require('./AbstractProcessors');
Html.prototype = new AbstractProcessors();

function Html()
{
    // add type of processing data
    this.entityType = 'html';

    // external modules declaration
    const cheerio = require('cheerio');
    const async = require('async');
    const minify = require('html-minifier').minify;
    const fs = require('fs');

    // variable declaration
    const self = this;
    const minifySettings = {
        collapseWhitespace: true,
        //removeAttributeQuotes: true,
        removeComments: true,
    };
    const fields = {};
    const styles = {};
    const fonts = {};
    let $pages = {};

    // declaration methods for generate html for include js/css resources
    const generateBlock = {
        js: function(data) {
            let result = [];
            data = Array.isArray(data) ? data : [data];
            for (var i in data) {
                result.push('<script src="'+data[i]+'"></script>');
            }
            return result.join('');
        },
        css: function(data) {
            let result = [];
            data = Array.isArray(data) ? data : [data];
            for (var i in data) {
                result.push('<link href="'+data[i]+'" rel="stylesheet">');
            }
            return result.join('');
        }
    };

    // getter for generate custom class names
    // used to replace inline styles to the class name
    const getStyleClass = function(style) {
        let styleKey = style.replace(/\s/g, '').split(';').sort().join('');
        if ('undefined' == typeof styles[styleKey]) {
            styles[styleKey] = self.config.generateClassName + '-' + Object.keys(styles).length;
            self.processors.Style.add('.'+styles[styleKey] + ' { '+style+' }');
        }
        return styles[styleKey];
    };

    /**
     * Building page Html
     * @return Promise
     */
    const buildPage = function(page)
    {
        return new Promise(function(resolve, reject) {
            // load main template
            let $ = page.$;
            let contentBlocksByName = {};
            let contentBlocksById = {};

            const bIncludeBuilder = function() {
                // processing tags `b-include` (used for include content data to the body of html)
                $('b-include').each(function(i, elem) {
                    let $el = $(this);

                    const rm = function() {
                        $el.remove();
                        console.error('Content Block', name, 'does not exists (removed)')
                    };

                    const name =  ($el.attr('blocktype')||'').toUpperCase();
                    const id =  $el.attr(':blockid');
                    let contentBlocksArr = 'BODY' == name ? [page.html_content] : [];
                    if (id && 'undefined' != typeof contentBlocksById[id]) {
                        contentBlocksArr.push(contentBlocksById[id].html_content || contentBlocksById[id].content)
                    } else if (name && 'undefined' != typeof contentBlocksByName[name]) {
                        for (var i in contentBlocksByName[name]) {
                            contentBlocksArr.push(contentBlocksByName[name][i])
                        }
                    }

                    if(contentBlocksArr.length) {
                        $el.replaceWith(contentBlocksArr.join(''));
                    } else {
                        rm();
                    }
                });

                if ( $('b-include').length) {
                    bIncludeBuilder();
                }
            };

            // starting building html contents
            // prepare content blocks
            for (let i in self._items) {
                let item = self._items[i];

                let name = (item.name || 'BODY').toUpperCase();
                if ('undefined' == typeof contentBlocksByName[name]) {
                    contentBlocksByName[name] = [];
                }
                contentBlocksByName[name].push(item.content);

                if (item.id) {
                    contentBlocksById[item.id] = item;
                }
            }
            bIncludeBuilder();

            // processing generate-block (used for include resource files to the html)
            $('generate-block').each(function(i, elem) {
                const name =  $(this).attr('name');
                const nameArr = name.split('_');

                if ('undefined' != typeof self.config.src[name] && 'undefined' != typeof nameArr[1] &&  'undefined' != typeof generateBlock[nameArr[1]]) {
                    if($(this).attr('selector')) {
                        $($(this).attr('selector')).append( generateBlock[nameArr[1]](self.config.src[name]) );
                        $(this).remove();
                    } else {
                        $(this).replaceWith(generateBlock[nameArr[1]](self.config.src[name]));
                    }
                } else {
                    $(this).remove();
                    console.error('Generate Block', name, 'does not exists (removed)')
                }
            });

            // processing data of custom attributes
            $('[generate-param]').each(function(i, elem) {
                const param = $(this).attr('generate-param');
                const paramArr = param.split(':');
                const paramValue = page[paramArr[0]] || fields[paramArr[0]] || false;

                if (param && paramValue) {
                    if ('undefined' != typeof paramArr[1] && paramArr[1]) {
                        $(this).attr(paramArr[1], paramValue);
                    } else {
                        $(this).html(paramValue);
                    }
                } else {
                    console.error('Generate Param', param, 'does not exists (ignored)')
                }
                $(this).removeAttr('generate-param');
            });

            // clearing inline styles
            $('[style]').each(function(i, elem) {
                let cls = getStyleClass( $(this).attr('style') );
                $(this).addClass(cls);
                $(this).removeAttr('style');
            });

            // preparing image path
            $('img').each(function(i, elem) {
                let src = self.imagePath($(this).attr('src'), self.config.src.internal_image, {
                    height: parseInt($(this).attr('height')),
                    width: parseInt($(this).attr('width'))
                });

                if (self.config.extraModules && self.config.extraModules.indexOf('LazyLoad') >=0) {
                    $(this).removeAttr('src');
                    $(this).attr('data-echo', src);
                } else {
                    $(this).attr('src', src);
                }
            });

            // save html contents
            self._save($.html()).then(resolve, reject);
        });
    };

    /**
     * Used for storing fonts
     */
    this.addFont = function(fontFamily, fontWeight) {
        if (!fontFamily) {
            return false;
        }

        const fontFamilyArr = fontFamily.split(',');
        if (fontFamilyArr.length > 1) {
            for (let i in fontFamilyArr) {
                self.addFont(fontFamilyArr[i], fontWeight);
            }
            return null;
        }
        // filter for default fonts
        switch (fontFamily) {
            case 'serif':
            case 'sans-serif':
            case 'monospace':
            case 'cursive':
            case 'fantasy':
            case 'system-ui':
                break;
            default:
                const key = fontFamily?fontFamily:'_all';
                if ('undefined' == typeof fonts[key]) {
                    fonts[key] = {};
                }

                if (fontWeight) {
                    fonts[key][fontWeight] = true;
                }
                break;
        }
    };

    /**
     * add custom fields
     * @param string name
     * @param string val
     * @return this
     */
    this.addField = function(name, val) {
        fields[name] = val;
        return this;
    };


    /**
     * add Page
     * @param object page
     * @return this
     */
    this.addPage = function(page) {
        if (page.file_path) {
            page.file_name = page.file_path+'.html';
            $pages[page.file_path] = page;
            self.processors.S3.addPage(page);
        }
        return this;
    };

    /**
     * Building Html
     * @return Promise
     */
    this.build = function()
    {
        console.info('Building Html');
        return new Promise(function(resolve, reject) {
            async.eachOfLimit($pages, 10, function(page, iteration, asyncCb) {

                // load main template
                const templateContents = fs.readFileSync(self.config.template, 'utf8');
                page.$ = cheerio.load(templateContents);

                buildPage(page).then(function(){

                    // save html contents
                    self._save(page.$.html(), false, page.file_path+'.html').then(function(){
                        asyncCb();
                    }, reject);
                }, reject)

            }, function() {
                resolve();
            });
        });
    };

    /**
     * Minify and save html
     * @return Promise
     */
    this.save = function() {

        //sorting custom fonts
        if ('undefined' != typeof fonts._all) {
            for(let f in fonts) {
                for(let size in fonts._all) {
                    fonts[f][size] = true;
                }
            }
            delete fonts._all;
        }

        //generation html block for custom fonts
        let fontsHtmlArr = [];
        for(let f in fonts) {
            let fontWeight = Object.keys(fonts[f]).length ? ':'+ Object.keys(fonts[f]).join(',') : false;
            fontsHtmlArr.push('<link href="//fonts.googleapis.com/css?family='+f+(fontWeight?fontWeight:'')+'&lang=en" rel="stylesheet" />');
        }

        return new Promise(function(resolve, reject) {
            let errors = [];
            async.eachOfLimit($pages, 10, function(page, iteration, asyncCb) {
                //add fonts to the page
                page.$('head meta').last().after(fontsHtmlArr.join(''));

                // minify and save results
                let generatedHtml = false;

                try {
                    generatedHtml = minify(page.$.html(), minifySettings);
                } catch (e) {
                    errors.push('Html structure error on the file'+ page.file_name +'(ignore minification)');
                    asyncCb();
                    return null;
                }

                self._save(generatedHtml, false, page.file_name).then(function() {
                    asyncCb();
                }, function(err){
                    errors.push(err);
                    asyncCb();
                });

            }, function() {
                if (!errors.length) {
                    resolve();
                } else {
                    reject({text:errors});
                }
            });
        });
    };
}

module.exports = Html;
