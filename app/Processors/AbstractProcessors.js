
/**
 * Class: AbstractProcessors
 * Extends: Abstract
 *
 * @property string _builded
 * @property array _items
 *
 * @method init
 * @method _save
 * @method save
 * @method imagePath
 * @method build
 */


var Abstract = require('../Abstract');
AbstractProcessors.prototype = new Abstract();
function AbstractProcessors() { }

/**
 * Merged text(html,js,css) results
 * @var string
 */
AbstractProcessors.prototype._builded = '';

/**
 * type of processing data
 * @var string|bool
 */
AbstractProcessors.prototype.entityType = false;

/**
 * iterator for webpack files
 * @var int
 */
AbstractProcessors.prototype.webpackFileI = 0;

/**
 * Initiation processor methods
 * @return this
 */
AbstractProcessors.prototype.init = function() {
    const self = this;
    self._items = [];

    /**
     * Initiation processor method for add items
     * * @return this
     */
    self.add = function(data) {
        self._items.push(data);
        return this;
    };
    return this;
};

/**
 * Save Text data(html,js,css) (used us protected method)
 * @return Promise
 */
AbstractProcessors.prototype._save = function(data, forWebpack, filePath) {
    const configSrcKey = 'internal_'+this.entityType;
    const parsePath = require("parse-filepath");
    const mkdirp = require('mkdirp');
    const fs = require('fs');
    const self = this;

    return new Promise(function(resolve, reject) {
        if (self.entityType &&  'undefined' != typeof self.config.src[configSrcKey] ) {
            //generate path to the file (base mode)
            let pathToFile = self.config.output + (filePath || self.config.src[configSrcKey]);
            let file = parsePath(pathToFile);

            //generate path to the file (Webpack mode)
            if (forWebpack) {
                pathToFile = self.config.webpackDist +(self.webpackFileI++) +'-'+ file.basename;
                file = parsePath(pathToFile);
                file.webpackDist = pathToFile;
            }

            //create folder
            mkdirp(file.dirname, function(){
                //save file
                fs.writeFile(pathToFile, data, function(err) {
                    if(err) {
                        reject(err)
                        return null;
                    }
                    self.processors.Style.addEntity(pathToFile, self.entityType);
                    resolve(file);
                });
            });
        } else {
            resolve();
        }
    });
};

/**
 * Save Webpack data(html,js,css)
 * @return Promise
 */
AbstractProcessors.prototype.saveForWebpack = function(data) {
    const self = this;
    return new Promise(function(resolve, reject) {
        self._save((data||self._builded), true).then(function(file){
            // add data for processing in the Webpack
            self.processors.Webpack.add(file);
            resolve(file);
        }, reject);
    });
};

/**
 * Save Text data(html,js,css)
 * @return Promise
 */
AbstractProcessors.prototype.save = function() {
    return this._save(this._builded);
};

/**
 * Add image for minify, generate new path
 * @param string src url to the source image
 * @return string new image url
 */
AbstractProcessors.prototype.imagePath = function(src, internalPath, size) {
    const parsePath = require("parse-filepath");
    const md5 = require('md5');
    const mkdirp = require('mkdirp');
    const self = this;

    // prepare file information by src
    let file = parsePath(src);
    size = size ? size : {};

    let sizeArr = [];

    //checking width
    if (size.width) {
        sizeArr.push(size.width);
    }

    //checking height
    if (size.height) {
        sizeArr.push(size.height);
    }

    var sizeStr = sizeArr.length ? '-'+sizeArr.join('x') : '';

    //generate path to the image file cache folder
    const hash = md5(file.path);
    let tmpImgPath = self.config.imgCache+hash.split('').slice(0, 5).join('/')+'/';
    //creating folder for image file cache
    mkdirp.sync(tmpImgPath);

    // prepare file local information
    file.local = {
        //new name of the directory for save
        directory: this.config.src.internal_image,
        //new name of the file
        filename: file.name + sizeStr + file.ext,
        // object of image width and height
        size:size,
        //path to the image file cache file
        tmpImg:tmpImgPath + self.config.siteId+'-'+hash + file.ext,
    };

    // add image for procexssing
    this.processors.Images.add(file);
    return this.config.publicPath + internalPath + file.local.filename;
};

/**
 * Build and minify Text data(html,js,css)
 * @return Promise
 */
AbstractProcessors.prototype.build = function() {
    return new Promise(function(resolve, reject) {
        resolve();
    });
};

module.exports = AbstractProcessors;