
/**
 * Class: Abstract
 *
 * @property object config
 * @property object processors
 *
 * @method setConfig
 * @method setProcessors
 * @method build
 */

function Abstract() { }

/**
 * Global config
 * @var object
 */
Abstract.prototype.config = {};

/**
 * Initiated processors
 * @var object
 */
Abstract.prototype.processors = {};

/**
 * Setter for global config object
 * @param object config
 * @return this
 */
Abstract.prototype.setConfig = function(config) {
    this.config = config;
    for(i in this.processors) {
        this.processors[i].config = config;
    }
    return this;
};

/**
 * Setter for processor object
 * @param object processor
 * @return this
 */
Abstract.prototype.setProcessors = function(processors) {
    this.processors = processors;
    for(let i in processors) {
        processors[i].processors = processors;
    }
    return this;
};

module.exports = Abstract;
