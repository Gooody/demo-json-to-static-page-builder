
/**
 * Class: Bootstrap
 * Extends: Abstract
 *
 * @param object cnf
 *
 * @method build
 */


var Abstract = require('./Abstract');
Bootstrap.prototype = new Abstract();

function Bootstrap(cnf)
{
    const self = this;

    // external modules declaration
    const fs = require('fs');
    const rimraf = require('rimraf');
    const recursive = require("recursive-readdir");
    const parsePath = require("parse-filepath");
    const async = require('async');

    // processor modules declaration
    const WebpackProcessor = require('./Processors/Webpack');
    const StyleProcessor = require('./Processors/Style');
    const JsProcessor = require('./Processors/Js');
    const HtmlProcessor = require('./Processors/Html');
    const ImagesProcessor = require('./Processors/Images');
    const RobotsProcessor = require('./Processors/Robots');
    const S3Processor = require('./Processors/S3');

    // initiation processors
    const processors = {
        Html:new HtmlProcessor().init(),
        Js: new JsProcessor().init(),
        Style: new StyleProcessor().init(),
        Images: new ImagesProcessor().init(),
        Webpack: new WebpackProcessor().init(),
        S3: new S3Processor(),
    };

    this.setProcessors(processors);
    this.setConfig(cnf);
    processors.S3.init();

    // initiation method for sorting data by type(html,js or css)
    this.setContent = function(type, content, name) {

        let parseJson = function(){
            let json ={};
            if ('string' == typeof content) {
                try {
                    json = JSON.parse(content);
                } catch (e){
                    console.error('Error parsing JSON', name, e);
                }
            } else {
                json =content;
            }
            return json;
        };

        switch (type.toLowerCase()) {
            case 'json':
                let json =parseJson();
                if (json.html_content) {
                    processors.Html.add({
                        name: json.type || 'BODY',
                        content: json.html_content,
                        id: json.id,
                    });
                }

                if (json.custom_script) {
                    processors.Js.add(json.custom_script);
                }

                if (json.custom_style) {
                    processors.Style.add(json.custom_style);
                }

                if (json.pages) {
                    for (let i in json.pages) {
                        processors.Html.addPage(json.pages[i]);
                    }
                }

                for (let field in json) {
                    if (json[field]) {
                        processors.Html.addField(field, json[field]);
                    }
                }
                break;
            case 'pages':
                processors.Html.addPage(parseJson());
                break;
            case 'html':
                processors.Html.add({
                    name: name,
                    content: content,
                });
                break;
            case 'css':
                processors.Style.add(content);
                break;
            case 'js':
                processors.Js.add(content);
                break;
        }
    };

    /**
     * Build, minify and save data
     * @return this
     */
    this.build = function()
    {
        return new Promise(function(resolve, reject) {
            let errors = [];
            // building data start
            async.eachOfLimit(processors, 1, function(processor, iteration, asyncCb) {
                processor.build().then(function(){
                    asyncCb();
                }, function(processorErr){
                    asyncCb();
                    errors.push(processorErr);
                });
                // building data end
            }, function() {
                // saving data start
                async.eachOfLimit(processors, 4, function(processor, iteration, asyncCb) {
                    processor.save().then(function(){
                        asyncCb();
                    }, function(processorErr){
                        errors.push(processorErr);
                        asyncCb();
                    });
                    // saving data end
                }, function() {
                    if(errors.length) {
                        reject(errors);
                    } else {
                        resolve();
                    }
                });
            });
        });
    };

    /**
     * Start processing
     * @return this
     */
    this.run = function() {
        return new Promise(function(resolve, reject) {
            // recursive reading all html,js,css for processing
            if (cnf.input) {
                recursive(cnf.input, function (err, files) {
                    if(err) {
                        return console.error(err);
                    }
                    async.eachOfLimit(files, 5, function(filesPath, iteration, asyncCb) {
                        let file = parsePath(filesPath);
                        fs.readFile(file.path, 'utf8', function(err, fileContents) {
                            self.setContent(file.ext.replace(/\./g, ''), fileContents, file.name);
                            asyncCb();
                        });
                    }, function() {
                        resolve(this);
                    });
                });
            } else {
                reject('no input data');
            }
        });
    };

    /**
     * Save data to S3
     * @return Promise
     */
    this.pushToS3 = function() {
        return processors.S3.push();
    };

    /**
     * Clear tmp data
     */
    this.clear = function() {
        setTimeout(function(){
            rimraf.sync(cnf.input);
            rimraf.sync(cnf.output);
        }, 1000);
    }

}

module.exports = Bootstrap;