
##Installation instructions:
1. clone repo
2. check path '{path_to_app}/tmp' is writable
3. add S3 ENV
4. run
    > npm install

##Running

1. run via npm
    > npm run dev
2. run via pm2

2.1. configure pm2-config.json

     {
        "apps":[
            {
                "name"              : "custom-html-page-builder",
                "script"            : "./app.js",
                "cwd"               : "./",
                 "error_file"        : "/opt/_logs/custom-html-page-builder-err.log",
                 "out_file"          : "/opt/_logs/custom-html-page-builder-out.log",
                "log_date_format"   : "YYYY-MM-DD HH:mm",
                "env":
                {
                      "UPLOADS":"",
                      "INPUT":"",
                      "OUTPUT":"",
                      "WEBPACK":"",
                      "WEBPACK_MODE":"",
                      "TEMPLATE":"",
                      "GENERATED_CLASS_NAME":"",
                      "HTML_PATH":"",
                      "JS_PATH":"",
                      "CSS_PATH":"",
                      "IMAGE_PATH":"",
                      "S3_ACCESS_KEY":"",
                      "S3_SECRET":"",
                      "DEV_ONLY":"",
                      "PORT":""
                }
            }
        ]
    }
2.2. run

    > pm2 start pm2-config.json


##Environment variables

* `UPLOADS` - path to the directory for upload files;   *default value:'{path_to_app}/tmp/uploads/'*
* `INPUT`   - path to the directory for input files;    *default value:'{path_to_app}/tmp/input/'*
* `OUTPUT`  - path to the directory for output files;   *default value:'{path_to_app}/tmp/output/'*
* `WEBPACK` - path to the directory for tmp-webpack files;   *default value:'{path_to_app}/tmp/webpack/'*
* `WEBPACK_MODE`  - param 'mode' for webpack ('production'||'development');   *default value:'production'*
* `TEMPLATE`  - set default template file name; *default value:'base.html'*; (the file should be in './templates/')
* `GENERATED_CLASS_NAME` - default generated class name (used for replace inline styles); *default value:'custom-generated'*
* `HTML_PATH` - path to the generated html-file; *default value:'index.html'*
* `JS_PATH` - path to the generated js-files; *default value:'js/view.js'*
* `CSS_PATH` - path to the generated css-files; *default value:'css/custom.css'*
* `IMAGE_PATH` - path to the rendeted image-files;  *default value:'image/'*
* `S3_ACCESS_KEY` - **Access Key for S3 - **required value**
* `S3_SECRET` - **Secret for S3 - **required value**
* `DEV_ONLY` - (1|0), 1-enable dev mode(do not sync and do not remove generated data);  *default value:0*
* `PORT`  - listened port;   *default value:3000*
* `IMG_CACHE`  - path to the directory for image cache files*
* `JSON_STORAGE_URL`  - url to all json*



##Routes
POST(/):
####POST-params:
* extra_modules[] - array of include extra modules to extend the functionality of the page
* public_path - used to correctly generate urls for js, css, images;  *default value:'' (empty string)*
* s3_bucket - Bucket name for S3 - **required value**
* s3_prefix - Prefix name for S3; *default value:'' (empty string)*
* html_path - reset path to the generated html-file; *default value:'index.html'*, or ENV
* generated_class_name - reset default generated class name (used for replace inline styles); *default value:'custom-generated'*, or ENV
* js_path - reset path to the generated js-files; *default value:'js/view.js'*, or ENV
* css_path - reset path to the generated css-files; *default value:'js/view.js'*, or ENV
* image_path - reset path to the rendeted image-files;  *default value:'image/'*, or ENV
* external_js[] - array of external js libs
* external_css[] - array of external css libs

* page[] - array of json-stringified page data
* json[] - array of json-stringified block data
* css[] - array of custom css code
* js[] - array of custom js code
* html[{name}][] - array of additional data blocks ({name} = body, footer, header, etc). if the data is already exists - will be added under the main data
   **Example:**
   > `html[footer][] = '<footer> Second part of the footer </footer>'`
* UPLOADED FILES
    * file_json[] - array of additional json-files page data
    * file_html[] - array of additional html-files block (file name = name of the block)
    * file_css[] - array of additional css-files
    * file_js[] - array of additional js-files

>_**see test-form.html for familiarize with POST-params structure**_


GET(/deploy-site/:siteId):
####GET-params:
* siteId - Site Identifier








