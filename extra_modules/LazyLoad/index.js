
function LazyLoad () {
    var echo = require("imports-loader?this=>window!echo-js");
    echo.init({
        offset: 100,
        throttle: 250,
        unload: false,
        callback: function (element, op) {
            console.log(element, 'has been', op + 'ed')
        }
    });
};

module.exports = LazyLoad;


